# ChessPiecesOnline

## Goal of the project

Replace chess.com current sprites by custom one (in this case pixel art one)

## To download

The plugin Stylus for navigators such as chrome or mozilla

## Sprites

Downloaded from https://opengameart.org/content/pixel-chess-pieces
